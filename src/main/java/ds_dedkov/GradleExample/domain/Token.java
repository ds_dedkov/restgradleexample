package ds_dedkov.GradleExample.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Token {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 1500)
    private String value;

    private Date expirationTime;

    public Token(String value, Date expirationTime) {
        this.value = value;
        this.expirationTime = expirationTime;
    }

    public Token(String value) {
        this.value = value;
    }

}
