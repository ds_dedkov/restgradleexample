package ds_dedkov.GradleExample.repository;

import ds_dedkov.GradleExample.domain.Token;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.Date;

public interface TokenRepository extends JpaRepository<Token,Long> {
    Token findByValue(String value);
    Token deleteByValue(String value);
    @Transactional
    void deleteAllByExpirationTimeBefore(Date time);
}
