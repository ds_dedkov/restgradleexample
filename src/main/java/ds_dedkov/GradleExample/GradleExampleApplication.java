package ds_dedkov.GradleExample;

import ds_dedkov.GradleExample.domain.Person;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.cloud.stream.messaging.Source;
@EnableBinding(Processor.class)
@SpringBootApplication
public class GradleExampleApplication {


	public static void main(String[] args) {
		SpringApplication.run(GradleExampleApplication.class, args);
	}

	@StreamListener(Processor.INPUT)
	public void handle(Person person) {
		System.out.println("Received: " + person);
	}


}
