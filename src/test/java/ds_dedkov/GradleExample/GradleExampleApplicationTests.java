package ds_dedkov.GradleExample;

import ds_dedkov.GradleExample.domain.Person;
import ds_dedkov.GradleExample.domain.Role;
import ds_dedkov.GradleExample.repository.PersonRepository;
import ds_dedkov.GradleExample.service.TokenProvider;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc(addFilters = false)
class GradleExampleApplicationTests {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private PersonRepository personRepository;
    @MockBean
    private TokenProvider provider;



    @Test
    public void getAllPerson() throws Exception {
        Person expected = new Person("login", "email", null, null);
        when(personRepository.findAll()).thenReturn(Collections.singletonList(expected));
        mockMvc.perform(get("/users/getAll/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is(expected.getName())));
        verify(personRepository, times(1)).findAll();

    }

    @Test
    public void getPersonById() throws Exception {
        Person expected = new Person("name", "password", null, null);
        when(personRepository.findById(1L)).thenReturn(Optional.of(expected));
        mockMvc.perform(get("/users/get/{id}", 1)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void createPerson() throws Exception {

        Person expected = new Person("name", "password", null, null);
        when(personRepository.save(any(Person.class))).thenReturn(expected);
        mockMvc.perform(post("/users/add")
                .content("{\"name\":\"name\" ,\"password\":\"password\"}")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

    }


    @Test
    public void updatePerson() throws Exception {
        Person expected = new Person("name", "password", null, null);
        when(personRepository.findById((long) 1)).thenReturn(Optional.of(expected));
        mockMvc.perform(put("/users/update/{id}",1)
                .content("{\"name\":\"name\" ,\"password\":\"password\"}")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    public void deletePerson() throws Exception {
        Person expected = new Person("name", "email", null, Role.ADMIN);
        when(personRepository.findById(1L)).thenReturn(Optional.of(expected));
        mockMvc.perform(delete("/users/delete/{id}", 1)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}

