package ds_dedkov.GradleExample.service;

import ds_dedkov.GradleExample.domain.Person;
import ds_dedkov.GradleExample.domain.Role;
import ds_dedkov.GradleExample.repository.PersonRepository;
import ds_dedkov.GradleExample.repository.TokenRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc(addFilters = false)
public class TokenProviderTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    PersonRepository personRepository;

    @Autowired
    TokenProvider provider;


    private Person person;


    @Test
    public void generateToken() throws Exception {
        person = new Person("login", "password", "name", Role.ADMIN);
        person.setEmail("email");
        person.setTokens(Collections.emptySet());
        person.setId(1L);
        String token = provider.generateToken(person).getValue();
        assertNotNull(token);



    }

    @Test
    public void getPersonById() throws Exception {
        person = new Person("login", "password", "name", Role.ADMIN);
        person.setEmail("email");
        person.setTokens(Collections.emptySet());
        person.setId(1L);
        String token = provider.generateToken(person).getValue();
        Person person = provider.getPersonById(token);
//        assertNotNull(person);


    }

    @Test
    public void validate() {
        person = new Person("login", "password", "name", Role.ADMIN);
        person.setEmail("email");
        person.setTokens(Collections.emptySet());
        person.setId(1L);
        String token = provider.generateToken(person).getValue();
        Boolean validate = provider.validate(token);

        assertTrue(validate);
    }
}